namespace TestEncryptionEF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EncryptionModel : DbContext
    {
        public EncryptionModel()
            : base("name=EncryptionModel")
        {
        }

        public virtual DbSet<CompteUtilisateur> CompteUtilisateur { get; set; }
        public virtual DbSet<Personne> Personne { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        { 
            modelBuilder.Entity<Personne>()
                .HasMany(e => e.CompteUtilisateur)
                .WithRequired(e => e.Personne)
                .WillCascadeOnDelete(false);
        }
    }
}
