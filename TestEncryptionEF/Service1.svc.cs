﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TestEncryptionEF
{
    public class Service1 : IService1
    {

        //Objet de relation avec la base de données (Entity Framework)
        EncryptionModel db = new EncryptionModel();

        /// <summary>
        /// Méthode ajoutant une personne
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        /// <param name="dateNaiss"></param>
        /// <returns></returns>
        public int AddPersonne(string nom, string prenom, DateTime dateNaiss)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Méthode retournant une personne à partir de son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Personne GetPersonneById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Méthode retournant toutes les personnes du nom passé en paramètre
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public List<Personne> GetPersonneByName(string nom)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Méthode retournant toutes les personnes
        /// </summary>
        /// <returns></returns>
        public List<Personne> GetPersonnes()
        {
            throw new NotImplementedException();
        }
    }
}
