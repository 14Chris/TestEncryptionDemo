namespace TestEncryptionEF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    [Table("CompteUtilisateur")]
    [DataContract(IsReference = true)]
    public partial class CompteUtilisateur
    {
        [Key]
        [DataMember]
        public int id_compte_user { get; set; }

        [DataMember]
        public int id_personne { get; set; }

        [Required]
        [StringLength(100)]
        [DataMember]
        public string login { get; set; }

        [Required]
        [StringLength(5000)]
        [DataMember]
        public string password { get; set; }

        [DataMember]
        public virtual Personne Personne { get; set; }
    }
}
