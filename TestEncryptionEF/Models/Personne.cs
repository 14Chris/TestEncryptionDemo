using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace TestEncryptionEF
{

    [Table("Personne")]
    [DataContract(IsReference = true)]
    public partial class Personne
    {
        public Personne()
        {
            CompteUtilisateur = new List<CompteUtilisateur>();
        }

        [Key]
        [DataMember]
        public int id_personne { get; set; }

        [Required]
        [StringLength(100)]
        [DataMember]
        public string nom { get; set; }

        [Required]
        [StringLength(100)]
        [DataMember]
        public string prenom { get; set; }

        [Required]
        [DataMember]
        public DateTime date_naissance { get; set; }

        [DataMember]
        public List<CompteUtilisateur> CompteUtilisateur { get; set; }

    }
}
